/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols locals = new LocalSymbols();
	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}

	protected String newSymbol(String name, Integer nr) {
		name += nr.toString();
		if(nr > 0)
		{
			locals.newSymbol(name);
		}
		else
		{
			globals.newSymbol(name);
		}
		return name;
	}
	
	protected String hasSymbol(String name, Integer nr) {

		if(locals.hasSymbol(name + nr))
			return name + nr;
		else if (globals.hasSymbol(name + 0))
			return name + 0;
		else 
			throw new RuntimeException("Variable " + name +" doesn't exists!");
	}
	
	
	protected void enterScope() {
		locals.enterScope();
	}
	
	protected void leaveScope() {
		locals.leaveScope();
	}
	
	
}
