tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer localScopeCnt = 0;
  Integer currentScopeNr = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;
    
zakres : ^(BEGIN {currentScopeNr++; enterScope();} (e+=zakres | e+=expr | d+=decl)* {currentScopeNr--;leaveScope();}) -> blok(wyr={$e},dekl={$d})
;    
                                                                      
decl  :
        ^(VAR i1=ID) -> dek(n={newSymbol($ID.text, currentScopeNr)})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                                      -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                                      -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                                      -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                                      -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr){hasSymbol($i1.text, currentScopeNr);}-> podstaw(p1={hasSymbol($i1.text, currentScopeNr)},p2={$e2.st})
        | INT{numer++;}                                                 -> int(i={$INT.text},j={numer.toString()})
        | i1=ID {hasSymbol($i1.text,currentScopeNr);}                   -> id(n={hasSymbol($i1.text, currentScopeNr)})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}